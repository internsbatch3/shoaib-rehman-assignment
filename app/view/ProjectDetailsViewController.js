/*
 * File: app/view/ProjectDetailsViewController.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 5.0.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 5.0.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('AssignmentProject.view.ProjectDetailsViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.projectdetails',

    save: function(button, e, eOpts) {
        debugger;
        var window = button.up();
        var values = window.getViewModel().data.teamdata;
        var action = window.getViewModel().data.action;

        var newValue = Ext.create('AssignmentProject.model.Team');
        var newProject = Ext.create('AssignmentProject.model.Project');

        if(action==='add')
        {

            newValue.set(values);

            newValue.set('Project',['Shoaib']);
            newValue.data.Project.push(newProject);

            var store = Ext.getStore('TeamStore');
            store.insert(0,newValue);
            store.sync();

        }

        if(action==='edit')
        {

            var store = Ext.getStore('TeamStore');
            store.update();
            store.sync();

        }

        if(action==='dbclick')
        {

            var store = Ext.getStore('TeamStore');
            store.update();
            store.sync();

        }

    },

    cancel: function(button, e, eOpts) {
        var window = button.up();
        window.close();
    }

});
